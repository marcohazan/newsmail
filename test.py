from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.message import Message
import smtplib
import gnupg
import time
import secrets
import hashlib
import unittest
from Objects.Channel import Channel
from Objects.News import News
from functions.config import Config
from Dao.newsmailDao import newsmailDao
from Dao.SentDao import SentDao
from Dao.ChannelDao import ChannelDao
from Dao.CanSendOnDao import CanSendOnDao
from Dao.SenderDao import SenderDao
import datetime
from functions.config import Config
from datetime import timedelta
from datetime import datetime


def generaId():
    unique = False
    msgid = None
    while not unique:
        token = secrets.token_bytes(16)
        milli_sec = int(round(time.time() * 1000))
        msgid = (str(token) + str(milli_sec)).encode("utf-8")
        msgid = hashlib.sha256(msgid).hexdigest()
        unique = newsmailDao.isUnique(msgid)
    return msgid


tester = Config.get("tester")
othertester = Config.get("tester2")
channeltest = 'testerchannel'
timetosleep = 5
mail_newsmail = Config.get("newsmail")

def sendMailSigned(subject,body,type,cc=''):
    basemsg = MIMEMultipart()
    basemsg.attach(MIMEText(body,type))
    gpg = gnupg.GPG(gnupghome = Config.get("pathtogpg"))
    basetext = basemsg.as_string().replace('\n', '\r\n')
    signature = str(gpg.sign(basetext, detach=True))
    signmsg = TestNews.messageFromSignature(signature)
    msg = MIMEMultipart(_subtype="signed", micalg="pgp-sha1",
    protocol="application/pgp-signature")
    msg['Subject'] = subject
    msg['From'] = tester + "t"
    msg['Cc'] = cc
    msg.attach(basemsg)
    msg.attach(signmsg)
    s = smtplib.SMTP("localhost")
    s.sendmail(tester+"t",[mail_newsmail], msg.as_string())
    s.quit()

def sendMail(subject,body,type):
    basemsg = MIMEMultipart()
    basemsg.attach(MIMEText(body,type))
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = tester + "t"
    msg.attach(basemsg)
    s = smtplib.SMTP("localhost")
    s.sendmail(tester + "t",[mail_newsmail], msg.as_string())
    s.quit()


class TestNews(unittest.TestCase):

    def messageFromSignature(signature):
        message = Message()
        message['Content-Type'] = 'application/pgp-signature; name="signature.asc"'
        message['Content-Description'] = 'OpenPGP digital signature'
        message.set_payload(signature)
        return message

    def test_createchannel(self):
        ChannelDao.delete(channeltest,tester)
        subject = 'Create channel'
        body = channeltest
        sendMailSigned(subject,body,'plain')
        time.sleep(timetosleep)
        ch = ChannelDao.getChannel(channeltest)
        self.assertIsNotNone(ch)
        self.assertTrue(ch.is_active)
        self.assertEqual(ch.owner,tester)
        self.assertEqual(ch.name,channeltest)
        #subject = 'Disable channel'
        #body = channeltest
        #Subject: [islab]{19/01/2022}Titolo News Prova
        #Body: Prova news
        #Content-type: text/plain
        #Firmata: SI

    def test_1(self):
        subject = '['+channeltest+']{22/04/2022}Titolo News Prova'
        body = 'Prova news'
        sendMailSigned(subject,body,'plain')
        time.sleep(timetosleep)
        news = newsmailDao.getLastByTitle('Titolo News Prova')
        self.assertEqual(newsmailDao.getStatus(news.msgid),2)
        newsmailDao.deleteNews(news.msgid)
        news2 = newsmailDao.getLastByTitle('Titolo News Prova')
        self.assertNotEqual(news2,news)
        self.assertIsNone(newsmailDao.get(news.msgid))
        sent_channels = SentDao.getChannels(news.msgid)
        self.assertEqual(0,len(sent_channels))

    #Subject: [islab]{19/01/2022}Titolo News Prova 2
    #Body: Prova news 2
    #Content-type: text/plain
    #Firmata: SI

    def test_2(self):
        subject = '['+channeltest+']{22/04/2022}Titolo News Prova 2'
        body = 'Prova news 2'
        sendMail(subject,body,'plain')
        time.sleep(timetosleep)
        news = newsmailDao.getLastByTitle('Titolo News Prova 2')
        self.assertEqual(newsmailDao.getStatus(news.msgid),1)
        sent_channels = SentDao.getChannels(news.msgid)
        self.assertEqual(len(sent_channels),1)
        self.assertEqual(sent_channels[0].name,channeltest)
        newsmailDao.deleteNews(news.msgid)
        news2 = newsmailDao.getLastByTitle('Titolo News Prova 2')
        self.assertNotEqual(news2,news)
        self.assertIsNone(newsmailDao.get(news.msgid))
        sent_channels = SentDao.getChannels(news.msgid)
        self.assertEqual(0,len(sent_channels))

    #Subject: [islab,newchannel]{19/01/2022}Titolo News Prova 3
    #Body: <p><b>Prova News</b></p>
    #Content-type: text/html
    #Firmata: SI
    def test_3(self):
        subject = '['+channeltest+',newchanneltester]{19/01/2022}Titolo News Prova 3'
        body = '<p><b>Prova News</b></p>'
        sendMailSigned(subject,body,'html')
        time.sleep(timetosleep)
        news = newsmailDao.getLastByTitle('Titolo News Prova 3')
        self.assertEqual(newsmailDao.getStatus(news.msgid),2)
        newchannel = ChannelDao.getChannel('newchanneltester')
        self.assertIsNotNone(newchannel)
        self.assertEqual(newchannel.name,'newchanneltester')
        self.assertEqual(newchannel.owner,tester)
        sent_channels = SentDao.getChannels(news.msgid)
        self.assertEqual(len(sent_channels),2)
        self.assertEqual(sent_channels[0].name,channeltest)
        newsmailDao.deleteNews(news.msgid)
        news2 = newsmailDao.getLastByTitle('Titolo News Prova 3')
        self.assertNotEqual(news2,news)
        self.assertIsNone(newsmailDao.get(news.msgid))
        sent_channels = SentDao.getChannels(news.msgid)
        self.assertEqual(0,len(sent_channels))
        sendMailSigned('Delete channel','newchanneltester','plain')
        time.sleep(timetosleep)
        newchannel = ChannelDao.getChannel('newchanneltester')
        self.assertIsNone(newchannel)



    def test_4(self):
        SenderDao.insert(othertester)
        othertesterchannel = Channel('othertesterchannel',True,othertester)
        ChannelDao.insert(othertesterchannel)
        subject = '['+channeltest+','+othertesterchannel.name+']Titolo News Prova 4'
        body = '# Prova news'
        sendMail(subject,body,'plain')
        time.sleep(timetosleep)
        news = newsmailDao.getLastByTitle('Titolo News Prova 4')
        self.assertEqual(newsmailDao.getStatus(news.msgid),1)
        subject = 'confirm news '+ news.msgid
        sendMail(subject,'','plain')
        time.sleep(timetosleep)
        news = newsmailDao.getLastByTitle('Titolo News Prova 4')
        self.assertEqual(newsmailDao.getStatus(news.msgid),2)
        published_channels = SentDao.getPublishedChannels(news.msgid)
        self.assertEqual(len(published_channels),1)
        self.assertEqual(published_channels[0].name,channeltest)
        unpublished_channels = SentDao.getUnPublishedChannels(news.msgid)
        self.assertEqual(len(unpublished_channels),1)
        self.assertEqual(unpublished_channels[0].name,othertesterchannel.name)
        sendMail('delete news '+news.msgid,'','plain')
        time.sleep(timetosleep)
        self.assertIsNone(newsmailDao.get(news.msgid))
        ChannelDao.delete(othertesterchannel.name,othertester)

    def test_5(self):
        SenderDao.insert(othertester,is_active = True)
        self.assertFalse(CanSendOnDao.check(othertester,channeltest))
        subject = 'Enable user on channel'
        cc = othertester
        body = channeltest
        sendMailSigned(subject,body,'plain',cc)
        time.sleep(timetosleep)
        self.assertTrue(CanSendOnDao.check(othertester,channeltest))
        subject = 'Disable user on channel'
        cc = othertester
        body = channeltest
        sendMailSigned(subject,body,'plain',cc)
        time.sleep(timetosleep)
        self.assertFalse(CanSendOnDao.check(othertester,channeltest))
        SenderDao.delete(othertester)

    def test_signup(self):
        publicaccess = Config.get("publicaccess")
        Config.set("publicaccess",True)
        testertosign = Config.get("tester2")
        s = smtplib.SMTP("localhost")
        basemsg = MIMEMultipart()
        basemsg.attach(MIMEText('Tester ToSign','plain'))
        msg = MIMEMultipart()
        msg['Subject'] = 'Sign up'
        msg['From'] = testertosign + "t"
        msg.attach(basemsg)
        s.sendmail(testertosign + "t",[mail_newsmail], msg.as_string())
        s.quit()
        time.sleep(timetosleep)
        self.assertIsNotNone(SenderDao.getId(testertosign))
        names = SenderDao.getNames(testertosign)
        self.assertEqual(names[0],'Tester')
        self.assertEqual(names[1],'ToSign')
        self.assertFalse(SenderDao.isActive(testertosign))
        authcode = SenderDao.getAuthCode(testertosign)
        self.assertIsNotNone(authcode)
        basemsg = MIMEMultipart()
        basemsg.attach(MIMEText(authcode,'plain'))
        msg = MIMEMultipart()
        msg['Subject'] = 'confirm identity'
        msg['From'] = testertosign + "t"
        msg.attach(basemsg)
        s = smtplib.SMTP("localhost")
        s.sendmail(testertosign + "t",[mail_newsmail], msg.as_string())
        s.quit()
        time.sleep(timetosleep)
        self.assertTrue(SenderDao.isActive(testertosign))
        self.assertIsNotNone(SenderDao.getAuthCode(testertosign))
        Config.set("publicaccess",publicaccess)
        SenderDao.delete(testertosign)

    def test_6(self):
        subject = '['+channeltest+']Titolo News Prova 5'
        body = 'Prova news 5'
        sendMailSigned(subject,body,'plain')
        time.sleep(timetosleep)
        news = newsmailDao.getLastByTitle('Titolo News Prova 5')
        self.assertEqual(news.body,body)
        subject = 'update news '+news.msgid
        body = 'Update Prova news 5'
        sendMail(subject,body,'plain')
        time.sleep(timetosleep)
        news = newsmailDao.getLastByTitle('Titolo News Prova 5')
        self.assertEqual(news.body,body)
        subject = 'update title '+ news.msgid
        body = 'Update Titolo News Prova 5'
        sendMail(subject,body,'plain')
        time.sleep(timetosleep)
        news = newsmailDao.get(news.msgid)
        self.assertEqual(news.title,body)
        subject = 'update expiration_date '+ news.msgid
        body = '01/11/2022'
        sendMail(subject,body,'plain')
        time.sleep(timetosleep)
        news = newsmailDao.get(news.msgid)
        self.assertEqual(news.expiration_date.strftime("%d/%m/%Y"),"01/11/2022")
        newsmailDao.deleteNews(news.msgid)

    def test_7(self):
        SenderDao.insert(othertester,is_active = True)
        msgid = generaId()
        news = News(msgid,othertester,'Titolo News Prova 7','Corpo News Prova 7','<p>Corpo News Prova 7</p>',datetime.now(),None)
        newsmailDao.insert(news,True)
        SentDao.insert(msgid,channeltest,False)
        unpublished_channels = SentDao.getUnPublishedChannels(msgid)
        published_channels = SentDao.getPublishedChannels(msgid)
        self.assertTrue(SentDao.isSent(msgid,channeltest))
        self.assertEqual(len(published_channels),0)
        self.assertEqual(unpublished_channels[0].name,channeltest)
        subject = 'Enable publication once '+channeltest
        body = msgid[0:32]
        sendMailSigned(subject,body,'plain',cc=othertester)
        time.sleep(timetosleep)
        unpublished_channels = SentDao.getUnPublishedChannels(msgid)
        published_channels = SentDao.getPublishedChannels(msgid)
        self.assertTrue(SentDao.isSent(msgid,channeltest))
        self.assertEqual(len(unpublished_channels),0)
        self.assertEqual(published_channels[0].name,channeltest)
        self.assertFalse(CanSendOnDao.check(othertester,channeltest))
        newsmailDao.deleteNews(msgid)
        newsmailDao.insert(news,True)
        SentDao.insert(msgid,channeltest,False)
        subject = 'Enable publication always '+channeltest
        body = msgid[0:32]
        sendMailSigned(subject,body,'plain',cc=othertester)
        time.sleep(timetosleep)
        self.assertEqual(len(unpublished_channels),0)
        self.assertEqual(published_channels[0].name,channeltest)
        self.assertTrue(CanSendOnDao.check(othertester,channeltest))
        newsmailDao.deleteNews(msgid)
        newsmailDao.insert(news,True)
        SentDao.insert(msgid,channeltest,False)
        subject = 'Reject publication '+channeltest
        body = msgid[0:32]
        sendMailSigned(subject,body,'plain',cc=othertester)
        time.sleep(timetosleep)
        self.assertFalse(SentDao.isSent(msgid,channeltest))
        newsmailDao.deleteNews(msgid)
        SenderDao.delete(othertester)





if __name__ == '__main__':
    unittest.main()
